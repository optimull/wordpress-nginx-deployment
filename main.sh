#!/usr/bin/env bash
#
# This is a rudimentary deployment script for nginx stable / mysql 8 / php73 / wordpress latest
# 
# Usage: ./main.sh --install full <domain> <wp admin email> <wp admin user> <wp admin password>
#

# Declare Globals
FQDN=""
WP_ADMIN_EMAIL=""
WP_ADMIN_USER="$(tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1)"
WP_ADMIN_PASSWD="$(tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1)"
PHP_VERSION="7.3"
WP_VERSION="latest"
EFS_ADDR=""

# Generate random 30-character values
readonly MYSQL_PASSWD="$(tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1)"
readonly WP_MYSQL_PASSWD="$(tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1)"
readonly WP_MYSQL_USER="$(tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1)"
readonly WP_MYSQL_DATABASE="$(tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1)"

# Colors - cause they're fun...
NOCOLOR='\033[0m'
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'

# It's alive!
main() {
  if [[ $# -gt 1 ]]; then # Make sure a useful number of args have been supplied
    while true; do
    local test="$1"
      case "${test}" in
        --install|-i)
          shift 1
          local sanitized_args="$(echo "$@" | sed -e 's/ -.*//' | tr '[:upper:]' '[:lower:]')" # strip any other commands from string and switch case
          install_ $sanitized_args
        ;;
        --add|-a)
          shift 1
          local sanitized_args="$(echo "$@" | sed -e 's/ -.*//' | tr '[:upper:]' '[:lower:]')" # strip any other commands from string and switch case
          setup_site_config $sanitized_args
        ;;
        --remove|-r)
          shift 1
          readonly FQDN="$1"
          if [[ -n "${FQDN}" ]]; then
            remove_domain
          else
            usage
          fi
        ;;
        --ssl|-s)
          shift 1
          local sanitized_args="$(echo "$@" | sed -e 's/ -.*//' | tr '[:upper:]' '[:lower:]')" # strip any other commands from string and switch case
          ssl_ $sanitized_args
        ;;
        --test2)
          shift 1
          local sanitized_args="$(echo "$@" | sed -e 's/ -.*//' | tr '[:upper:]' '[:lower:]')" # strip any other commands from string and switch case
          echo $sanitized_args
        ;;
      esac
    shift 1

    # break the loop once no args are left to test
    if [[ -z "${test}" ]]; then
      break
    fi

    done
  else
    usage
  fi
}

#######################################
# Process install subcommand args
# Globals:
#   None
# Arguements:
#   $*
# Returns:
#   None
#######################################
install_() {
  local option="$1"
  shift 1 # shift args 1 position to get rid of unneeded args

  case "$option" in
    nginx) install_nginx;;
    mysql8) install_mysql8;;
    certbot) install_certbot;;
    acmesh) install_acmesh;;
    mod-http-cache-purge) install_mod_http_cache_purge;;
    php)
      # $1:(php version)
      readonly PHP_VERSION="$1"

      if [[ -n "$1" ]]; then
        install_php
      else
        usage
      fi
    ;;
    wpcli) install_wpcli;;
    full)
      # $1:(domain) $2:(wp admin email) $3:(wp admin user) $4:(wp admin password) $5:(php version) $6:(wordpress version)
      readonly FQDN="$1"
      readonly WP_ADMIN_EMAIL="$2"
      readonly WP_ADMIN_USER="${3:-$WP_ADMIN_USER}"
      readonly WP_ADMIN_PASSWD="${4:-$WP_ADMIN_PASSWD}"
      readonly PHP_VERSION="${5:-$PHP_VERSION}"
      readonly WP_VERSION="${6:-$WP_VERSION}"

      if [[ -n "$1" && -n "$2" ]]; then
        install_full
      else
        usage
      fi
    ;;
    test)
      if [[ -n "$1" ]]; then
      printf "$*\n"
      fi
    ;;
    *) echo "Unknown install option...";;
  esac
}

#######################################
# Process ssl subcommand args
# Globals:
#   None
# Arguements:
#   $*
# Returns:
#   None
#######################################
ssl_() {
  local option="$1"
  shift 1 # shift args 1 position to get rid of unneeded args

  case "$option" in
    certbot)
      readonly FQDN="$1"
      readonly WP_ADMIN_EMAIL="$2"
      if [[ -n "${FQDN}" && -n "${WP_ADMIN_EMAIL}" ]]; then
        setup_certbot_certificate && \
        setup_ssl_site_config
      else
        usage
      fi
      ;;
    certbot-digitalocean)
      readonly FQDN="$1"
      readonly WP_ADMIN_EMAIL="$2"
      if [[ -n "${FQDN}" && -n "${WP_ADMIN_EMAIL}" ]]; then
        setup_certbot_certificate_digitalocean && \
        setup_ssl_site_config
      else
        usage
      fi
      ;;
    acmesh-cloudflare)
      readonly FQDN="$1"
      readonly CF_Token="$2"
      readonly CF_Account_ID="$3"
      readonly CF_Zone_ID="$4"
      if [[ -n "${FQDN}" && -n "${CF_Token}" && -n "${CF_Account_ID}" && -n "${CF_Zone_ID}" ]]; then
        setup_acmesh_certificate_cloudflare && \
        setup_ssl_site_config_acmesh
      else
        usage
      fi
      ;;
    *) echo "Unknown install option...";;
  esac
}

#######################################
# Install everything
# Globals:
#   FQDN
#   WP_MYSQL_DATABASE
#   WP_MYSQL_USER
#   WP_MYSQL_PASSWD
#   PHP_VERSION
# Arguements:
#   None
# Returns:
#   None
#######################################
install_full() {
  initial_update && \
  install_nginx && \
  install_mod_http_cache_purge && \
  install_php && \
  setup_nginx_config && \
  setup_site_config && \
  install_mysql8 && \
  setup_database && \
  install_wpcli && \
  install_wordpress && \
  save_credentials
}

#######################################
# Display usage
# Globals:
#   None
# Arguements:
#   None
# Returns:
#   None
#######################################
usage() {
  printf "Usage: \n"
  printf "$0\t -i|--install full <domain> <wp admin email> [wp admin user] [wp admin password] [php version] - Install full LEMP stack with blank install for Domain\n"
  printf "\t\t -a|--add <domain>\t\t - Add Domain\n"
  printf "\t\t -r|--remove <domain>\t\t - Remove Domain\n"
  printf "\t\t -s|--ssl <type> <domain>\t\t - Setup SSL for Domain using NginX or DigitalOcean\n"
  printf "\t\t -p|--php <domain> <version>\t - Set PHP to Version\n"
}

#######################################
# Initial system update
# Globals:
#   None
# Arguements:
#   None
# Returns:
#   None
#######################################
initial_update() {
  apt-get -y update && apt-get -y upgrade && apt-get -y autoremove && \
  notice "System update completed." && \

  return 0 || \
  err "Unable to run initial update."
}

#######################################
# Install NginX
# Globals:
#   None
# Arguements:
#   None
# Returns:
#   None
#######################################
install_nginx() {
  add-apt-repository -y ppa:ondrej/nginx && \
  apt-get -y update && \
  apt-get -y install nginx-full && \
  notice "NginX installation completed." && \

  return 0 || \
  err "Unable to install NginX"
}

#######################################
# Install PHP
# Globals:
#   PHP_VERSION
# Arguements:
#   None
# Returns:
#   None
#######################################
install_php() {
  add-apt-repository -y ppa:ondrej/php && \
  apt-get -y update && \
  apt-get -y install php${PHP_VERSION}-cli php${PHP_VERSION}-common php${PHP_VERSION}-fpm php${PHP_VERSION}-mbstring php${PHP_VERSION}-opcache php${PHP_VERSION}-mysql php${PHP_VERSION}-zip && \
  notice "PHP installation completed." && \

  return 0 || \
  err "Unable to install PHP ${version}"
}

#######################################
# Install Certbot
# Globals:
#   None
# Arguements:
#   None
# Returns:
#   None
#######################################
install_certbot() {
  add-apt-repository -y universe && \
  add-apt-repository -y ppa:certbot/certbot && \
  apt-get -y update && \
  apt-get -y install certbot python-certbot-nginx && \
  notice "Certbot installation completed." && \

  return 0 || \
  err "Unable to install Certbot"
}

#######################################
# Install Acme.sh
# Globals:
#   None
# Arguements:
#   None
# Returns:
#   None
#######################################
install_acmesh() {
  curl https://get.acme.sh | sh && \
  notice "Acme.sh installation completed." && \

  return 0 || \
  err "Unable to install Acme.sh"
}

#######################################
# Setup NginX global configuration
# Globals:
#   None
# Arguements:
#   None
# Returns:
#   None
#######################################
setup_nginx_config() {
  mv /etc/nginx /etc/nginx.bak && \
  git clone https://gitlab.com/optimull/wordpress-nginx.git /etc/nginx && \

  # install catch all for unknown domains
  ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default && \

  # manually create directory to enable ability to use installed modules
  mkdir /etc/nginx/modules-enabled && \
  ln -s /usr/share/nginx/modules /etc/nginx/modules-available && \

  notice "Nginx configuration completed." && \

  return 0 || \
  err "Could not setup NginX config"
}

#######################################
# Setup site directories and configuration
# Globals:
#   FQDN
# Arguements:
#   $*
# Returns:
#   None
#######################################
setup_site_config() {
  # didn't set to readonly so we could loop through interations
  local FQDN="${1:-$FQDN}"

  if [[ -n "${FQDN}" ]]; then
    if [[ ! -f /etc/nginx/sites-enabled/${FQDN} && ! -d /sites/${FQDN} ]]; then
      # Create initial directories
      mkdir -p /sites/${FQDN}/{logs,public} && \
      chown -R www-data. /sites/${FQDN} && \

      # Copy example site to sites-enabled and replace the domain
      cp /etc/nginx/sites-available/fastcgi-cache.com /etc/nginx/sites-enabled/${FQDN} && \
      sed -i "s/fastcgi-cache.com/${FQDN}/g" /etc/nginx/sites-enabled/${FQDN} && \
      reload_nginx && \
      notice "Added site ${FQDN} successfully." && \

      return 0 || \
      err "Could not setup site config for ${FQDN}"
    else
      warn "Configuration for ${FQDN} already exists."
    fi
  else
    err "Could not setup site config for ${FQDN}"
  fi
}

#######################################
# Setup SSL site configuration
# Globals:
#   FQDN
# Arguements:
#   None
# Returns:
#   None
#######################################
setup_ssl_site_config() {
  # Copy example site to sites-enabled and replace the domain
  cp /etc/nginx/sites-available/ssl-fastcgi-cache.com /etc/nginx/sites-enabled/${FQDN} && \
  sed -i "s/ssl-fastcgi-cache.com/${FQDN}/g" /etc/nginx/sites-enabled/${FQDN} && \
  reload_nginx && \
  notice "Setup SSL configuration for ${FQDN}." && \

  return 0 || \
  err "Could not setup SSL site config"
}

#######################################
# Setup SSL site configuration
# Globals:
#   FQDN
# Arguements:
#   None
# Returns:
#   None
#######################################
setup_ssl_site_config_acmesh() {
  # Copy example site to sites-enabled and replace the domain
  cp /etc/nginx/sites-available/ssl-fastcgi-cache.com /etc/nginx/sites-enabled/${FQDN} && \
  sed -i "s/ssl-fastcgi-cache.com/${FQDN}/g" /etc/nginx/sites-enabled/${FQDN} && \
  sed -i "s|/etc/letsencrypt/live/${FQDN}/fullchain.pem|/etc/ssl/certs/${FQDN}.pem|g" /etc/nginx/sites-enabled/${FQDN} && \
  sed -i "s|/etc/letsencrypt/live/${FQDN}/privkey.pem|/etc/ssl/private/${FQDN}.pem|g" /etc/nginx/sites-enabled/${FQDN} && \
  reload_nginx && \
  notice "Setup SSL configuration for ${FQDN}." && \

  return 0 || \
  err "Could not setup SSL site config"
}

#######################################
# Install MySQL 8.0
# Globals:
#   MYSQL_PASSWD
# Arguements:
#   None
# Returns:
#   None
#######################################
install_mysql8() {
  # NOTE: might use this to install the key since retreiving the key manually via apt-key is giving inconsistent results
  # wget "https://dev.mysql.com/get/mysql-apt-config_0.8.14-1_all.deb" && \
  # sudo debconf-set-selections <<< "echo mysql-apt-config mysql-apt-config/select-server select mysql-8.0" && \
  # dpkg -i mysql-apt-config_0.8.14-1_all.deb && \
  sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com 5072E1F5 && \
  echo "deb http://repo.mysql.com/apt/ubuntu/ bionic mysql-8.0" | sudo tee /etc/apt/sources.list.d/mysql.list && \
  apt-get update && \
  sudo debconf-set-selections <<< "mysql-community-server mysql-community-server/root-pass password ${MYSQL_PASSWD}" && \
  sudo debconf-set-selections <<< "mysql-community-server mysql-community-server/re-root-pass password ${MYSQL_PASSWD}" && \
  sudo DEBIAN_FRONTEND=noninteractive apt-get -y install mysql-server && \
  echo -e "[client]\nuser=root\npassword=\"${MYSQL_PASSWD}\"" | tee ~/.my.cnf && \
  echo -e "[mysqld]\ndefault-authentication-plugin = mysql_native_password" | sudo tee /etc/mysql/mysql.conf.d/default-auth-override.cnf && \
  service mysql restart && \
  notice "MySQL installation completed." && \

  return 0 || \
  err "Could not setup MySQL 8"
}

#######################################
# Setup database for WordPress
# Globals:
#   MYSQL_PASSWD
#   WP_MYSQL_DATABASE
#   WP_MYSQL_USER
#   WP_MYSQL_PASSWD
# Arguements:
#   None
# Returns:
#   None
#######################################
setup_database() {
  mysql -u root -p${MYSQL_PASSWD} -e "
FLUSH PRIVILEGES;
CREATE DATABASE ${WP_MYSQL_DATABASE};
CREATE USER '${WP_MYSQL_USER}'@'localhost' IDENTIFIED BY '${WP_MYSQL_PASSWD}';
GRANT ALL PRIVILEGES ON ${WP_MYSQL_DATABASE}.* TO '${WP_MYSQL_USER}'@'localhost';
FLUSH PRIVILEGES;" && \
  notice "WordPress database setup completed." && \

  return 0 || \
  err "Could not setup MySQL database"
}

#######################################
# Install mod-http-cache-purge
# Globals:
#   None
# Arguements:
#   None
# Returns:
#   None
#######################################
install_mod_http_cache_purge() {
  apt -y install libnginx-mod-http-cache-purge && \
  sed -i '/^pid.*/a \\nload_module modules\/ngx_http_cache_purge_module.so;' /etc/nginx/nginx.conf && \
  notice "mod-http-cache-purge installation completed." && \

  return 0 || \
  err "Could not install mod-http-cache-purge."
}

#######################################
# Install WP-CLI
# Globals:
#   None
# Arguements:
#   None
# Returns:
#   None
#######################################
install_wpcli() {
  curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar && \
  mv wp-cli.phar /usr/local/bin/wp && \
  ln -s /usr/local/bin/wp /usr/bin/wp && \
  chmod 755 /usr/local/bin/wp && \
  notice "WP-CLI installation completed." && \

  return 0 || \
  err "Could not install WP-CLI"
}

#######################################
# Install WordPress Core
# Globals:
#   WP_MYSQL_DATABASE
#   WP_MYSQL_USER
#   WP_MYSQL_PASSWD
# Arguements:
#   None
# Returns:
#   None
#######################################
install_wordpress() {
  sudo -u www-data wp --path=/sites/${FQDN}/public core download --version="${WP_VERSION}" && \
  sudo -u www-data wp --path=/sites/${FQDN}/public config create --dbname="${WP_MYSQL_DATABASE}" --dbuser="${WP_MYSQL_USER}" --dbhost="127.0.0.1" --dbpass="${WP_MYSQL_PASSWD}" && \
  sudo -u www-data wp --path=/sites/${FQDN}/public core install --url="${FQDN}" --title="Wordpress Site" --admin_user="${WP_ADMIN_USER}" --admin_password="${WP_ADMIN_PASSWD}" --admin_email="${WP_ADMIN_EMAIL}" && \
  notice "Successfully installed WordPress for ${FQDN}." && \

  return 0 || \
  err "Could not install WordPress"
}

#######################################
# Install cachesfilesd and mount EFS
# Globals:
#   EFS_ADDR
#   FQDN
# Arguements:
#   None
# Returns:
#   None
# Note: Only ran if EFS_ADDR is set
#######################################
setup_efs() {
  if [ -n "${EFS_ADDR}" ]; then
    # Install cachefilesd and nfs-common
    apt-get -y install cachefilesd nfs-common && \

    # Create efs mount directory and mount efs
    mkdir -p /efs && \
    echo "${EFS_ADDR}:/ /efs nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,fsc,_netdev 0 0" | tee -a /etc/fstab && \
    mount -a

    # Copy uploads content, remove original dir, and create symlink to efs
    rsync -avzh /sites/${FQDN}/public/wp-content/uploads/ /efs && \
    rm -r /sites/${FQDN}/public/wp-content/uploads/ && \
    ln -fs /efs /sites/${FQDN}/public/wp-content/uploads && \
    chown -h www-data. /sites/${FQDN}/public/wp-content/uploads && \
    notice "EFS setup completed." && \
    
    return 0 || \
    err "Could not setup EFS"
  fi
}

#######################################
# Save auto-generated credentials to file
# Globals:
#   MYSQL_PASSWD
#   WP_MYSQL_DATABASE
#   WP_MYSQL_USER
#   WP_MYSQL_PASSWD
# Arguements:
#   None
# Returns:
#   None
#######################################
save_credentials() {
  echo -e "MySQL Root Password\n${MYSQL_PASSWD}\n\nWordPress Database Info\nDatabase: ${WP_MYSQL_DATABASE}\nUser: ${WP_MYSQL_USER}\nPass: ${WP_MYSQL_PASSWD}\n\nWordPress\nUser: ${WP_ADMIN_USER}\nPass: ${WP_ADMIN_PASSWD}" \
    | tee -a ~/credentials && \
  notice "Credentials saved to ~/credentials." && \
  
  return 0 || \
  err "Could not save credentials"
}

#######################################
# Generate certbot certificate for NginX
# Globals:
#   FQDN
#   WP_ADMIN_EMAIL
# Arguements:
#   None
# Returns:
#   None
#######################################
setup_certbot_certificate() {
  if [[ $(echo ${FQDN} | grep -o '\.' | wc -l) = 1 ]]; then
    certbot --non-interactive --agree-tos -m ${WP_ADMIN_EMAIL} --nginx -d ${FQDN} -d www.${FQDN} && \
    notice "SSL certificate request for ${FQDN} successful." && \

    return 0 || \
    err "Could not get SSL certificate for ${FQDN}"
  else
    certbot --non-interactive --agree-tos -m ${WP_ADMIN_EMAIL} --nginx -d ${FQDN} && \
    notice "SSL certificate request for ${FQDN} successful." && \

    return 0 || \
    err "Could not get SSL certificate for ${FQDN}"
  fi
}

#######################################
# Generate certbot certificate via DigitalOcean
# Globals:
#   FQDN
#   WP_ADMIN_EMAIL
# Arguements:
#   None
# Returns:
#   None
#######################################
setup_certbot_certificate_digitalocean() {
  if [[ $(echo ${FQDN} | grep -o '\.' | wc -l) = 1 ]]; then
    certbot certonly \
    --non-interactive \
    --agree-tos \
    -m ${WP_ADMIN_EMAIL} \
    --dns-digitalocean \
    --dns-digitalocean-credentials ~/.secrets/certbot/digitalocean.ini \
    --dns-digitalocean-propagation-seconds 10 \
    -d ${FQDN} -d www.${FQDN} && \
    notice "SSL certificate request for ${FQDN} successful." && \

    return 0 || \
    err "Could not get SSL certificate for ${FQDN}"
  else
    certbot certonly \
    --non-interactive \
    --agree-tos \
    -m ${WP_ADMIN_EMAIL} \
    --dns-digitalocean \
    --dns-digitalocean-credentials ~/.secrets/certbot/digitalocean.ini \
    --dns-digitalocean-propagation-seconds 10 \
    -d ${FQDN} && \
    notice "SSL certificate request for ${FQDN} successful." && \

    return 0 || \
    err "Could not get SSL certificate for ${FQDN}"
  fi
}

#######################################
# Generate acme.sh certificate for NginX
# Globals:
#   FQDN
#   WP_ADMIN_EMAIL
# Arguements:
#   None
# Returns:
#   None
#######################################
setup_acmesh_certificate_cloudflare() {
  if [[ $(echo ${FQDN} | grep -o '\.' | wc -l) = 1 ]]; then
    acme.sh --issue --dns dns_cf -d ${FQDN} -d www.${FQDN}
    acme.sh --install-cert -d ${FQDN} --key-file /etc/ssl/private/${FQDN}.pem --fullchain-file /etc/ssl/certs/${FQDN}.pem --reloadcmd "service nginx force-reload" && \
    notice "SSL certificate request for ${FQDN} successful." && \

    return 0 || \
    err "Could not get SSL certificate for ${FQDN}"
  else
    acme.sh --issue --dns dns_cf -d ${FQDN} && \
    acme.sh --install-cert -d ${FQDN} --key-file /etc/ssl/private/${FQDN}.pem --fullchain-file /etc/ssl/certs/${FQDN}.pem --reloadcmd "service nginx force-reload" && \
    notice "SSL certificate request for ${FQDN} successful." && \

    return 0 || \
    err "Could not get SSL certificate for ${FQDN}"
  fi
}

#######################################
# Remove domain
# Globals:
#   None
# Arguements:
#   FQDN
# Returns:
#   None
#######################################
remove_domain() {
    read -r -p "Are you sure? [y/N] " response
    if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]; then
      tar -C /sites/ -czvf ${FQDN}.tar.gz ${FQDN} && \
      rm -r /sites/"${FQDN}" && \
      rm /etc/nginx/sites-enabled/"${FQDN}" && \
      reload_nginx
    fi
}

#######################################
# Reload NginX
# Globals:
#   None
# Arguements:
#   None
# Returns:
#   None
#######################################
reload_nginx() {
  service nginx configtest && \
  service nginx reload && \
  notice "Successfully reloaded NginX." && \

  return 0 || \
  err "Could not reload NginX"
}

err() {
  echo -e "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: ${RED} $@ ${NOCOLOR}" >&2
  exit 1
}

warn() {
  echo -e "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: ${YELLOW} $@ ${NOCOLOR}" >&2
}

notice() {
  echo -e "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: ${GREEN} $@ ${NOCOLOR}" >&2
}

main "$@"
